//
//  Router.swift
//  alamofire-router-demo
//
//  Created by Jorge Jimenez on 1/12/17.
//  Copyright © 2017 Jorge Jimenez. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    static let baseURLString = "http://jsonplaceholder.typicode.com"
    
    case Get(Int)
    case Create([String: Any])
    case Delete(Int)
    
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .Get:
            return .get
        case .Create:
            return .post
        case .Delete:
            return .delete
        }
    }
    
    var path: String {
        switch self {
        case .Get(let postNumber):
            return ("posts/\(postNumber)")
        case .Create:
            return ("posts")
        case .Delete(let postNumber):
            return ("posts/\(postNumber)")
        }
    }
    
    func asURLRequest() throws -> URLRequest {

        let url = URL(string: Router.baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .Create(let parameters):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
        default:
            return urlRequest
        }
    }
    

}
