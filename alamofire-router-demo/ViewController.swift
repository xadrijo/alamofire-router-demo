//
//  ViewController.swift
//  alamofire-router-demo
//
//  Created by Jorge Jimenez on 1/12/17.
//  Copyright © 2017 Jorge Jimenez. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request(Router.Get(1))
            .responseJSON { (response) in
                guard response.result.error == nil else {
                    print("error calling GET on /posts/1")
                    print(response.result.error!)
                    return
                }
                
                if let value: Any = response.result.value {
                    let post = JSON(value)
                    print("The post is: " + post.description)
                    if let title = post["title"].string {
                        print("The title is: " + title)
                    } else {
                        print("error parsing /posts/1")
                    }
                }
        }
        
        /**********************************************************/
        
        let newPost = ["title": "Frist Psot", "body": "I iz fisrt", "userId": 1] as [String : Any]
        
        Alamofire.request(Router.Create(newPost))
            .responseJSON { (response) in
                guard response.result.error == nil else {
                    print("error doing POST on /posts")
                    print(response.result.error!)
                    return
                }
                
                if let value: Any = response.result.value {
                    let post = JSON(value)
                    print("**********************************")
                    print("The post is: " + post.description)
                }
        }
        
        /**********************************************************/
        
        Alamofire.request(Router.Delete(1))
            .responseJSON { (response) in
                if let error = response.result.error {
                    print("error calling DELETE on /posts/1")
                    print(error)
                }
                
                print("**********************************")
                print("\(response.result)")
        }
    }


}

